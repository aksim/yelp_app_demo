//
//  YelpApiClient.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/3/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//
import Foundation

class YelpApiClient {
    private static let apiKey:String = "GXkYXE7Fm3rVORSuDZaLQWvd4ptsCV3SFutQ5PN2zyGZANkA_ZEuyXKqfSRNZcWflJX8vOA2PRlioo3y_bjww_WHPPaaFZV0YCHhwL-a_lvvg-Lc_1Kn6xw75ebRW3Yx"
    private static let baseUrl = "https://api.yelp.com/v3/"
    private static let searchEndPoint = "businesses/search"
    static let shared = YelpApiClient()
    private init(){}
    
    
    func getBusiness(position:Position, completion: @escaping (YelpResponseBody?, Error?) -> Void){
        let url = YelpApiClient.baseUrl + YelpApiClient.searchEndPoint;
        let params = ["latitude":position.latitude, "longitude":position.longitude]
        do {
            let request = try prepareRequest(url: url, parameters: params, method: "GET")
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                guard let data  = data else { return }
                
                do {
                    let responseBody = try JSONDecoder().decode(YelpResponseBody.self, from: data)
                    completion(responseBody, nil)
                    
                }catch let jsonError {
                    completion(nil, jsonError)
                }
                
            }.resume()
            
        }catch let error {
            completion(nil, error)
        }
    }
    
    
    private func prepareRequest(url: String, parameters: [String: Any]?, method: String?) throws -> URLRequest {
        guard var components = URLComponents(string: url) else { throw YelpApiError.invalidUrl(message: "please check the url") }
        
        if let params = parameters {
            components.queryItems = params.map({ (arg) -> URLQueryItem in
                let (key, value) = arg
                return URLQueryItem(name: key, value: "\(value)")
            })
            
        }
        
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        var request = URLRequest(url: components.url!)
        //request.set
        request.httpMethod = method
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.setValue("Bearer \(YelpApiClient.apiKey)", forHTTPHeaderField:"Authorization")
        request.timeoutInterval = 60.0
        return request
    }
    
    enum YelpApiError: Error {
        case invalidUrl(message: String)
        case invalidMethod(message: String)
    }
}

//
//  YelpBusinessAnnotation.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/7/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit
import MapKit

class YelpBusinessAnnotation: NSObject, MKAnnotation {
    var business: Business
    var coordinate: CLLocationCoordinate2D { return CLLocationCoordinate2DMake(business.coordinates.latitude, business.coordinates.longitude) }
    
    init(business: Business, index:Int) {
        self.business = business
        super.init()
    }
    
    var title: String? {
        return business.name
    }
    
    var subtitle: String? {
        return business.name
    }
}

//
//  MapViewController.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/3/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    var mapView: MKMapView
    var markerIndex:Int
    
    
    init() {
        self.mapView = MKMapView()
        self.markerIndex = 0
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        self.setupViews()
        self.setupConstraints()
        
    }
    
    private func setupViews(){
        self.mapView.showsUserLocation = true
        self.mapView.backgroundColor = .white
        self.view.addSubview(self.mapView)
        
    }
    
    private func setupConstraints(){
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.mapView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.mapView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.mapView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.mapView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
    }
    
    func addAnnotations(businesses: [Business]){
        self.markerIndex = 0
        let annotations = businesses.map { (business) -> MKAnnotation in
            let annotation = YelpBusinessAnnotation(business: business, index: 1)
            return annotation
        }
        DispatchQueue.main.async {[weak self] in
            self?.mapView.addAnnotations(annotations)
        }
    }
    
    func updateRegion(region:MKCoordinateRegion){
        self.mapView.setRegion(region, animated: true)
    }
    
}



extension MapViewController: MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation { return nil }
        self.markerIndex += 1
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "customCallOutView")

        if annotationView == nil {
            let yelpAnnotationView = UIYelpBusinessAnnotationView(annotation: annotation, reuseIdentifier: "customCallOutView", index: self.markerIndex)
            return yelpAnnotationView
        }
        annotationView!.annotation = annotation
        return annotationView
    }
    

  

}

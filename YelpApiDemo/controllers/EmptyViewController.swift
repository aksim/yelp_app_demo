//
//  EmptyViewController.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/3/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit

class EmptyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        let message = UILabel()
        message.text = "Please leave me alone !!!";
        message.textColor = .lightGray
        self.view.addSubview(message)
        
        message.translatesAutoresizingMaskIntoConstraints = false
        message.widthAnchor.constraint(equalToConstant: 200).isActive = true
        message.heightAnchor.constraint(equalToConstant: 35).isActive = true
        message.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        message.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        
        
    }
    


}

//
//  BusinessDetailsViewController.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/9/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit

class BusinessDetailsViewController: UIViewController {

    let business:Business
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var headDetailsContainer: UIView!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rating: UIYelpRating!
    @IBOutlet weak var review: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var untilLabel: UILabel!
    
    
    init(business:Business){
        self.business = business
        super.init(nibName: "BusinessDetailsViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupContent()
    }
    
    private func setupViews(){
        self.back.imageView?.contentMode = .scaleAspectFit
        self.back.setImage(UIImage(named: "back")?.withRenderingMode(.alwaysTemplate) , for: .normal)
        self.headDetailsContainer.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.3).cgColor
        self.headDetailsContainer.layer.borderWidth = 0.3
    }
    
    
    private func setupContent(){
        self.titleLabel.text = business.name
        self.rating.updateRating(rating: business.rating)
        self.review.text = "\(business.review_count) Reviews"
        self.categoryLabel.text = business.categories.map({ (category) -> String in
            return category.title
        }).joined(separator: ", ")
        self.price.text = business.price
        if business.is_closed {
            self.status.text = "Close"
            self.untilLabel.text = "until 9:00 AM"
            self.status.textColor = UIColor(red:0.84, green:0.15, blue:0.11, alpha:1.0)
        }else{
            self.status.text =  "Open"
            self.untilLabel.text = "until 7:00 PM"
            self.status.textColor = UIColor(red:0.00, green:0.56, blue:0.32, alpha:1.0)

        }
        
    }

  
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

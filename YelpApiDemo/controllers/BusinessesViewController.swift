//
//  BusinessesViewController.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/3/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit

class BusinessesViewController: UIViewController {
    
    let tableView:UITableView
    let slideUpIndicator:UIView
    var businesses:[Business]
    var Ymin:CGFloat
    var Ymax:CGFloat
    
    
    init(Ymin:CGFloat, Ymax:CGFloat) {
        self.Ymin = Ymin
        self.Ymax = Ymax
        self.tableView = UITableView()
        self.slideUpIndicator = UIView()
        self.businesses = []
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.setupViews()
        self.setupConstraints()
        self.setupGesture()
        
    }
    
    
    
    private func setupViews(){
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "BusinessTableViewCell")
        self.tableView.register(UINib(nibName: "BusinessTableViewCell", bundle: nil), forCellReuseIdentifier: "BusinessTableViewCell")
        self.tableView.backgroundColor = .white
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.slideUpIndicator.backgroundColor = UIColor(red:0.77, green:0.77, blue:0.77, alpha:1.0)
        self.slideUpIndicator.layer.masksToBounds = true
        self.slideUpIndicator.layer.cornerRadius = 3
        self.view.addSubview(self.tableView)
        self.view.addSubview(self.slideUpIndicator)
        
        
    }
    
    private func setupGesture(){
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(self.panGesture))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
    }
    
    private func setupConstraints(){
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        self.slideUpIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.slideUpIndicator.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 10).isActive = true
        self.slideUpIndicator.heightAnchor.constraint(equalToConstant: 7).isActive = true
        self.slideUpIndicator.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.slideUpIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true

        
    }
    
    func reloadData(businesses: [Business]){
        DispatchQueue.main.async {  [weak self] in
            self?.businesses = businesses
            self?.tableView.reloadData()
        }
    }
    
    
}

extension BusinessesViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.businesses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessTableViewCell", for: indexPath) as! BusinessTableViewCell
        cell.updateWith(business: self.businesses[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: Notification.Name("OpenBusinessDetails"), object:nil, userInfo: ["seletedBusiness":self.businesses[indexPath.row]])
    }
    
}

extension BusinessesViewController: UIGestureRecognizerDelegate{
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let gesture = (gestureRecognizer as! UIPanGestureRecognizer)
        let direction = gesture.velocity(in: view).y
        
        let y = view.frame.minY
        if (y == Ymin && tableView.contentOffset.y == 0 && direction > 0) || (y == Ymax) {
            tableView.isScrollEnabled = false
        } else {
            tableView.isScrollEnabled = true
        }
        
        return false
    }
    
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
        
        let y = self.view.frame.minY
        if (y + translation.y >= Ymin) && (y + translation.y <= Ymax) {
            self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        
        if recognizer.state == .ended {
            var duration =  velocity.y < 0 ? Double((y - Ymin) / -velocity.y) : Double((Ymax - y) / velocity.y )
            
            duration = duration > 1.3 ? 1 : duration
            
            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
                if  velocity.y >= 0 {
                    self.view.frame = CGRect(x: 0, y: self.Ymax, width: self.view.frame.width, height: self.view.frame.height)
                } else {
                    self.view.frame = CGRect(x: 0, y: self.Ymin, width: self.view.frame.width, height: self.view.frame.height)
                }
                
            }, completion: { [weak self] _ in
                if ( velocity.y < 0 ) {
                    self?.tableView.isScrollEnabled = true
                }
            })
        }
    }
}

//
//  HomeViewController.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/2/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit
import MapKit


class HomeViewController: UITabBarController {
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewControllers = [searchViewController(), emptyViewController("Activity"), emptyViewController("Me"), emptyViewController("Collections"), emptyViewController("Plus")]
        self.tabBar.tintColor = .red
    }
    
    private func searchViewController() -> UIViewController {
        let searchController = SearchViewController()
        searchController.title = "Search"
        searchController.tabBarItem.image = UIImage(named: "search")
        searchController.tabBarItem.imageInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        return searchController
    }

    private func emptyViewController(_ title: String) -> UIViewController {
        let controller = EmptyViewController()
        controller.title = title
        controller.tabBarItem.image = UIImage(named: "identity")
        controller.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        return controller
    }

}


//
//  SearchViewController.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/3/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class SearchViewController: UIViewController {

    let headerContainer:UIView
    let searchField:UIYelpSearchField
    let mapViewController = MapViewController()
    var listViewController:BusinessesViewController!
    var safeAreaHeight:CGFloat = 0
    let regionInMeters: Double = 10000
    let locationManager = CLLocationManager()
    var currentLocation:CLLocationCoordinate2D?
    
    
    init(){
        self.headerContainer = UIView()
        self.searchField = UIYelpSearchField()
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let height = self.view.safeAreaLayoutGuide.layoutFrame.size.height
        let width  = view.frame.width
        self.listViewController.view.frame = CGRect(x: 0, y: self.view.frame.height*2/3, width: width, height: height)
        self.listViewController.Ymin = self.view.safeAreaInsets.top + 45
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupConstraints()
        self.setupListViewController()
        self.checkLocationServices()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showBusinessDetails(notification:)), name: Notification.Name("OpenBusinessDetails"), object: nil)
    }
    
    
    
    private func setupViews(){
        self.headerContainer.backgroundColor = .red
        self.headerContainer.addSubview(self.searchField)
        self.view.addSubview(self.headerContainer)
        self.addChild(self.mapViewController)
        self.view.addSubview(self.mapViewController.view)
        self.mapViewController.didMove(toParent: self)
        
    }
    
    private func setupConstraints(){
        let safe = view.safeAreaLayoutGuide
        self.headerContainer.translatesAutoresizingMaskIntoConstraints = false
        self.headerContainer.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.headerContainer.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.headerContainer.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.headerContainer.bottomAnchor.constraint(equalTo: safe.topAnchor, constant: 45).isActive = true
        
        self.searchField.translatesAutoresizingMaskIntoConstraints = false
        self.searchField.leftAnchor.constraint(equalTo: self.headerContainer.leftAnchor, constant: 8).isActive = true
        self.searchField.rightAnchor.constraint(equalTo: self.headerContainer.rightAnchor, constant: -8).isActive = true
        self.searchField.topAnchor.constraint(equalTo: safe.topAnchor, constant: 0).isActive = true
        self.searchField.bottomAnchor.constraint(equalTo: self.headerContainer.bottomAnchor, constant: -5).isActive = true
        
        self.mapViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.mapViewController.view.topAnchor.constraint(equalTo: self.headerContainer.bottomAnchor).isActive = true
        self.mapViewController.view.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.mapViewController.view.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.mapViewController.view.heightAnchor.constraint(equalToConstant: self.view.frame.height*2/3).isActive = true
        
    }

    
    private func setupListViewController() {
        self.listViewController = BusinessesViewController(Ymin: 88, Ymax: self.view.frame.height * 2/3)
        self.addChild(self.listViewController)
        self.view.addSubview(self.listViewController.view)
        self.listViewController.didMove(toParent: self)
        self.listViewController.view.layer.cornerRadius = 5
        self.listViewController.view.layer.masksToBounds = true
        self.listViewController.view.layer.borderWidth = 0.2
        self.listViewController.view.layer.borderColor = UIColor.darkGray.withAlphaComponent(03).cgColor
    }
    
    
    
    
    private func performSearch(cordonates: Position){
        print("performSearch")
        YelpApiClient.shared.getBusiness(position: cordonates) { (responseBody, responseError) in
            if let error = responseError{
                print("Somthing went wrong with Yelp api client", error)
            }
            
            if let response = responseBody{
                self.mapViewController.addAnnotations(businesses: response.businesses)
                self.listViewController.reloadData(businesses: response.businesses)
            }
        }
    }

    @objc func showBusinessDetails(notification: NSNotification){
        if let business = notification.userInfo?["seletedBusiness"] as? Business {
            let detailsViewController = BusinessDetailsViewController(business: business)
            self.navigationController?.pushViewController(detailsViewController, animated: true)
        }
    }
    
    
    
    
    
}


extension SearchViewController: CLLocationManagerDelegate {
    
    private func showAlertForPermission(message: String){
        // show Alert For Permisson or activation
    }
    
    private func centerViewOnUserLocation() {
        if let location = self.locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            self.mapViewController.updateRegion(region: region)
            self.currentLocation = region.center
            self.performSearch(cordonates: Position(latitude: region.center.latitude, longitude: region.center.longitude))
        }
    }
    
    private func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            checkLocationAuthorization()
        } else {
            self.showAlertForPermission(message: "activate")
        }
    }
    
    private func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            centerViewOnUserLocation()
            locationManager.startUpdatingLocation()
            break
        case .denied:
            self.showAlertForPermission(message: "permission")
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            self.showAlertForPermission(message: "permission")
            break
        case .authorizedAlways:
            break
        default:
            self.showAlertForPermission(message: "Error")
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        self.mapViewController.updateRegion(region: region)
        let distanceInMeters = distanceFromLastLocation(location: region.center)
        if distanceInMeters > 200 {
            self.currentLocation = region.center
            self.performSearch(cordonates: Position(latitude: region.center.latitude, longitude: region.center.longitude))
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.checkLocationAuthorization()
    }
    
    private func distanceFromLastLocation(location:CLLocationCoordinate2D) -> Double{
        if let position = self.currentLocation {
            let from = CLLocation(latitude: position.latitude, longitude: position.longitude)
            let to = CLLocation(latitude: location.latitude, longitude: location.longitude)
            return to.distance(from : from)
        }
        return 0
    }
}

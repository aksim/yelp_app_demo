//
//  Position.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/4/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

struct Position: Decodable {
    let latitude:Double
    let longitude:Double
}

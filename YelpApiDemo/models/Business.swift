//
//  Business.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/3/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//



struct Business : Decodable{
    let id:String
    let alias:String
    let name: String
    let image_url: String
    let is_closed: Bool
    let url: String
    let review_count: Int
    let rating:Double
    let coordinates: Position
    let categories:[Category]
    let price: String?
    let phone: String
    let display_phone:String
    let distance:Double
    let location:Address
    
    func getDistance() -> String {
        return "\(distance) mi"
    }

}



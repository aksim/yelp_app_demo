//
//  YelpResponseBody.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/3/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

struct YelpResponseBody : Decodable{
    let businesses: [Business]
    let total: Int
}

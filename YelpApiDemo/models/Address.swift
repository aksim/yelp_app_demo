//
//  Address.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/4/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

struct Address: Decodable {
    let address1:String
    let address2:String?
    let address3:String?
    let city:String
    let zip_code:String
    let country:String
    let state:String
    let display_address: [String]
}

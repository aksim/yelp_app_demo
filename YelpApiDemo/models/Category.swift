//
//  Category.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/6/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit

struct Category: Decodable{
    let alias:String
    let title:String
}

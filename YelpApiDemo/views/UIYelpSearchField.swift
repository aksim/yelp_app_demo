//
//  UIYelpSearchField.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/3/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit

class UIYelpSearchField: UITextField {

    init(){
        super.init(frame: CGRect.init())
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(frame: CGRect.init())
        self.setupView()
    }
    
    private func setupView(){
        self.backgroundColor = .white
        self.layer.cornerRadius = 3
        self.placeholder = "Search ..."
        self.textColor = .darkGray
        self.font = self.font?.withSize(14)
        
        
        let back = setupIcon("back", rect: CGRect(x: 0, y: 0, width: 50, height: 20))
        self.leftView = back
        self.leftViewMode = .always
        
        let map = setupIcon("map", rect: CGRect(x:40, y: 0, width: 25, height: 25))
        let filter = setupIcon("filter", rect: CGRect(x: 0, y: 0, width: 25, height: 25))
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 85, height: 25))
        rightView.addSubview(filter)
        rightView.addSubview(map)
        self.rightView = rightView
        self.rightViewMode = .always
        

    }
    
    private func setupIcon(_ icon: String, rect: CGRect) -> UIView{
        let view = UIView(frame: rect)
        let imageView:UIImageView = UIImageView(frame: CGRect(x: 5, y: 0, width: rect.width, height: rect.height))
        imageView.image = UIImage(named: icon)?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .darkGray
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        return view
    }
    
    
}

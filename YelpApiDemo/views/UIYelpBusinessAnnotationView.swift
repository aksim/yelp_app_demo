//
//  UIYelpBusinessAnnotationView.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/7/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//
import UIKit
import MapKit

class UIYelpBusinessAnnotationView: MKAnnotationView{
    // data
    let index:Int
    let indexLabel:UILabel
    weak var customCalloutView: UIBusinessMapView?
    
    
    override var annotation: MKAnnotation? {
        willSet { customCalloutView?.removeFromSuperview() }
    }
    
    init(annotation: MKAnnotation?, reuseIdentifier: String?, index:Int){
        self.index = index
        self.indexLabel = UILabel(frame: CGRect(x:0, y:0, width:15, height: 15))
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        self.canShowCallout = false // 1
        self.contentMode = .scaleAspectFit
        self.image = UIImage(named: "pinMarker")
        self.indexLabel.textColor = .white
        self.indexLabel.font = UIFont.systemFont(ofSize: 9, weight: .bold)
        self.indexLabel.textAlignment = .center
        self.indexLabel.text = "\(self.index)"
        self.addSubview(self.indexLabel)
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            self.customCalloutView?.removeFromSuperview() // remove old custom callout (if any)
            
            if let newCustomCalloutView = loadBusinessMapView() {
                
                // adjust position of the custom callout.
                newCustomCalloutView.frame.origin.x -= newCustomCalloutView.frame.width / 2.0 - (self.frame.width / 2.0)
                newCustomCalloutView.frame.origin.y -= (newCustomCalloutView.frame.height - 10)
                
 
                self.addSubview(newCustomCalloutView)
                self.customCalloutView = newCustomCalloutView
                
                // animate presentation
                if animated {
                    self.customCalloutView!.alpha = 0.0
                    UIView.animate(withDuration: 0.2, animations: {
                        self.image = UIImage(named: "dotMarker")
                        self.indexLabel.alpha = 0.0
                        self.customCalloutView!.alpha = 1.0
                    })
                }
            }
        } else {
            
            if let customView = customCalloutView {
                customView.removeFromSuperview()
                if animated { // fade out animation, then remove it.
                    UIView.animate(withDuration: 0.2, animations: {
                        self.image = UIImage(named: "pinMarker")
                        self.indexLabel.alpha = 1.0
                        customView.alpha = 0.0
                    }, completion: { (success) in
                        customView.removeFromSuperview()
                    })
                }
            }
        }
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if let customView =  self.customCalloutView{
            let isInside = customView.frame.contains(point)
            return isInside;
        }
        return false
    }
    
    func loadBusinessMapView() -> UIBusinessMapView? {
        if let views = Bundle.main.loadNibNamed("UIBusinessMapView", owner: self, options: nil) as? [UIBusinessMapView], views.count > 0 {
            let businessMapView = views.first!
            //personDetailMapView.delegate = self.personDetailDelegate
            if let businessAnnotation = annotation as? YelpBusinessAnnotation {
                let business = businessAnnotation.business
                businessMapView.setupWith(business: business)
                businessMapView.reloadData()
            }
            return businessMapView
        }
        return nil
    }
    

    

}

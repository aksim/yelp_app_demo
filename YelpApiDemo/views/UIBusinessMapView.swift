//
//  UIBusinessMapView.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/7/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit


class UIBusinessMapView: UIView {
    
    var business:Business?
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var rating: UIYelpRating!
    @IBOutlet weak var review: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.container.layer.cornerRadius = 5
        self.container.layer.masksToBounds = true
        self.container.layer.borderWidth = 0.2
        self.container.layer.borderColor = UIColor.darkGray.withAlphaComponent(03).cgColor
        
    }
    
    
    func setupWith(business: Business) {
        self.business = business
    }
    
    func reloadData(){
        if let b = business {
            self.name.text = b.name
            self.country.text = b.location.country
            self.review.text = "\(b.review_count) Reviews"
            self.price.text = b.price
            self.rating.updateRating(rating: b.rating)
        }
    }
    
  
    @IBAction func showDetails(_ sender: UIButton) {
        if let b = business{
            NotificationCenter.default.post(name: Notification.Name("OpenBusinessDetails"), object:nil, userInfo: ["seletedBusiness":b])
        }
    }
    
}

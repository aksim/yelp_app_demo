//
//  BusinessTableViewCell.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/3/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit

class BusinessTableViewCell: UITableViewCell {
    

    @IBOutlet weak var thumabnail: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var rating: UIYelpRating!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var categories: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var reviews: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.thumabnail.layer.cornerRadius = 10

    }
    
    func updateWith(business:Business){
        self.title.text = business.name
        self.thumabnail.image = UIImage(named: "thumbnail")
        self.address.text = business.location.address1
        //self.categories.text = business.categories.jo
        self.categories.text = business.categories.map({ (category) -> String in return category.title}).joined(separator: ", ")
        self.price.text = business.price
        self.distance.text = business.getDistance()
        self.rating.updateRating(rating: business.rating)
        self.reviews.text = "\(business.review_count) Reviews"

    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
}

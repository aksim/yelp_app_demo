//
//  UIYelpRating.swift
//  YelpApiDemo
//
//  Created by Omar Aksim on 5/3/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import UIKit

class UIYelpRating: UIStackView {

    
    func updateRating(rating: Double){
        
        let starColor:String = rating > 3.5 ? "red":"yellow"
        
        for (index,star) in self.subviews.enumerated() {
            let rateStar = star as! UIImageView
            if( Double(index + 1) <= rating ){
                rateStar.image = UIImage(named: "\(starColor)Star")
            }else if( Double(index) < rating ){
                rateStar.image = UIImage(named: "\(starColor)GrayStar")
            }else{
                rateStar.image = UIImage(named: "grayStar")
            }
        }
    }
    
    
 

}

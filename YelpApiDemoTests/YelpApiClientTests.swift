//
//  YelpApiClientTests.swift
//  YelpApiDemoTests
//
//  Created by Omar Aksim on 5/8/19.
//  Copyright © 2019 Omar Aksim. All rights reserved.
//

import XCTest
@testable import YelpApiDemo

class YelpApiClientTests: XCTestCase {
    
    func testApiCoientShouldReturnResults() {
        let promise = expectation(description: "Api Client should give results when the location is valid")
        YelpApiClient.shared.getBusiness(position: Position(latitude: 42.487260, longitude: -79.335400)) { (body, error) in
            if let result = body {
                XCTAssertTrue(result.businesses.count > 0)
                XCTAssertTrue(result.total > 0)
                promise.fulfill()
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    
    func testApiClientShouldNotResturnResults() {
        let promise = expectation(description: "Api Client should give results when the location is not valid")
        YelpApiClient.shared.getBusiness(position: Position(latitude: 33.582591, longitude: -31.925383)) { (body, error) in
            if let result = body {
                XCTAssertTrue(result.businesses.count == 0)
                XCTAssertTrue(result.total == 0)
                promise.fulfill()
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
}

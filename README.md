# YelpApiDemo: Assignment

small app that shows places around you on a list also inside the map using Yelp APIS

## Getting Started

The project contains two principal screens:
* First screen: No xib file used for this screen except for cells and custom views, this screen also contains two main components
    * The map (MapKit)
    * Sliding List (tableView)
* Second Screen: built using xib file attached to a UIViewController 

### Runing

clone the repository -> open the xcodeproj file -> run the app



